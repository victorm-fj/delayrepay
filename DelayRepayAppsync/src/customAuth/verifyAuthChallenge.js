export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, undefined, 2));
	if (
		event.request.privateChallengeParameters.answer ===
		event.request.challengeAnswer
	) {
		event.response.answerCorrect = true;
	} else {
		event.response.answerCorrect = false;
	}
	callback(null, event);
};
