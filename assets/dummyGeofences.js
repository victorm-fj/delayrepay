const geofences = [
	{
		identifier: 'Crown Point Shopping Park',
		radius: 100,
		latitude: 53.7885435,
		longitude: -1.5431357,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1231,
		},
	},
	{
		identifier: 'Leeds Train Station',
		radius: 100,
		latitude: 53.794436,
		longitude: -1.549775,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1232,
		},
	},
	{
		identifier: 'CityPark Leeds Dock',
		radius: 100,
		latitude: 53.7904588,
		longitude: -1.5326785,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1233,
		},
	},
	{
		identifier: 'McDonald\'s',
		radius: 100,
		latitude: 53.7821645,
		longitude: -1.5300907,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1234,
		},
	},
	{
		identifier: 'Leeds Costco',
		radius: 100,
		latitude: 53.7863793,
		longitude: -1.5399718,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1235,
		},
	},
	{
		identifier: 'Leeds Audi',
		radius: 100,
		latitude: 53.785968,
		longitude: -1.5449932,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1236,
		},
	},
	{
		identifier: 'Beaver Works',
		radius: 100,
		latitude: 53.7836452,
		longitude: -1.5364554,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1237,
		},
	},
	{
		identifier: 'Eiger Music Studios',
		radius: 100,
		latitude: 53.7836084,
		longitude: -1.5441214,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1238,
		},
	},
	// Start - Freeway Drive POIS - simulator testing only //
	{
		identifier: 'The Markham Apartments',
		radius: 100,
		latitude: 37.335907,
		longitude: -122.038879,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1239,
		},
	},
	{
		identifier: 'Near Garden Gate Scholl',
		radius: 100,
		latitude: 37.333433,
		longitude: -122.045409,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1230,
		},
	},
	{
		identifier: 'Pocatello Avenue',
		radius: 100,
		latitude: 37.333638,
		longitude: -122.052837,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1241,
		},
	},
	{
		identifier: 'Caroline Drive',
		radius: 100,
		latitude: 37.333092,
		longitude: -122.060651,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1242,
		},
	},
	{
		identifier: 'Deodora Drive',
		radius: 100,
		latitude: 37.334632,
		longitude: -122.06951,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1243,
		},
	},
	{
		identifier: 'Junipero Serra Freeway',
		radius: 100,
		latitude: 37.334951,
		longitude: -122.078276,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1244,
		},
	},
	{
		identifier: 'The Forum Sn Antonio',
		radius: 100,
		latitude: 37.337272,
		longitude: -122.08673,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1245,
		},
	},
	// End - Freeway Drive POIS - simulator testing only //

	// Start - City Run POIS - simulator testing only //
	{
		identifier: 'Apple, Inc.',
		radius: 100,
		latitude: 37.330946,
		longitude: -122.03063,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1246,
		},
	},
	{
		identifier: 'Apple, Inc.',
		radius: 50,
		latitude: 37.330946,
		longitude: -122.03063,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1246,
		},
	},
	{
		identifier: 'Mariani Avenue',
		radius: 50,
		latitude: 37.330702,
		longitude: -122.029574,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1247,
		},
	},
	{
		identifier: 'Merrit Drive',
		radius: 50,
		latitude: 37.330358,
		longitude: -122.028412,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1248,
		},
	},
	{
		identifier: 'Sam H. Lawson Middle Scholl',
		radius: 100,
		latitude: 37.329799,
		longitude: -122.027169,
		notifyOnEntry: true,
		notifyOnExit: false,
		notifyOnDwell: false,
		loiteringDelay: 30000, // 30 seconds
		extras: {
			// Optional arbitrary meta-data
			zone_id: 1249,
		},
	},
	// End - City Run POIS - simulator testing only //
];