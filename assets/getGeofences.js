const fs = require('fs');

const stations = require('./stations');

const locations = stations.StationList.Station.map(station => ({
  identifier: station.Name,
  radius: 100,
  longitude: Number(station.Longitude),
  latitude: Number(station.Latitude),
  notifyOnEntry: true,
  notifyOnExit: false,
  notifyOnDwell: false,
  loiteringDelay: 30000,
}));

fs.writeFile('geofences.js', `export default ${JSON.stringify(locations)}`, {}, (err) => {
  if (err) {
    console.log(err);
  }
});

