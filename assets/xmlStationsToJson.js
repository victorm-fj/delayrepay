const parser = require('xml2json');
const fs = require('fs');

fs.readFile('./stations.xml', 'utf8', function(err, data) {
  const json = parser.toJson(data);
  fs.writeFile('stations.js', `module.exports = ${json}`, {}, (err) => {
    if (err) {
      console.log(err);
    }
  });
});
