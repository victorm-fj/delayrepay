// @flow
import React from 'react';
import { View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import StepIndicator from 'react-native-step-indicator';
import { Button } from 'react-native-elements';

import RecognitionScanNav from './RecognitionScanNav';
import ImageResult from '../screens/imageResult/ImageResult';
import TextDetection from '../screens/textDetection/TextDetection';
import styles, { stepIndicatorStyles } from './styles';

export default createStackNavigator(
	{
		RecognitionScan: { screen: RecognitionScanNav },
		ImageResult: { screen: ImageResult },
		TextDetection: { screen: TextDetection },
	},
	{
		initialRouteName: 'RecognitionScan',
		navigationOptions: ({ navigation }) => {
			const {
				state: { routeName },
			} = navigation;
			let index = 0;
			let headerLeftAction = () =>
				navigation.navigate('Bot', { continue: true });
			let headerRightAction = () => navigation.navigate('ImageResult');
			if (routeName === 'ImageResult') {
				const onImageContinue = navigation.getParam('onImageContinue');
				index = 1;
				headerLeftAction = () => navigation.replace('RecognitionScan');
				headerRightAction = onImageContinue;
			} else if (routeName === 'TextDetection') {
				index = 2;
				headerLeftAction = () => navigation.replace('RecognitionScan');
				headerRightAction = () =>
					navigation.navigate('Bot', { continue: true });
			}
			const headerLeft = (
				<Button
					title="Back"
					titleStyle={styles.buttonTitleStyle}
					onPress={headerLeftAction}
					clear
				/>
			);
			const headerTitle = (
				<View style={styles.stepIndicatorContainer}>
					<StepIndicator
						currentPosition={index}
						customStyles={stepIndicatorStyles}
						stepCount={3}
					/>
				</View>
			);
			const headerRight = (
				<Button
					title="Continue"
					titleStyle={styles.buttonTitleStyle}
					onPress={headerRightAction}
					clear
				/>
			);
			return { headerLeft, headerTitle, headerRight };
		},
	}
);
