import { createSwitchNavigator } from 'react-navigation';

import LoadScan from '../screens/recognition/LoadScan';
import Recognition from '../screens/recognition/Recognition';
import DocScanner from '../screens/recognition/DocScanner';

export default createSwitchNavigator(
	{
		LoadScan: { screen: LoadScan },
		Recognition: { screen: Recognition },
		DocScanner: { screen: DocScanner },
	},
	{ initialRouteName: 'LoadScan' }
);
