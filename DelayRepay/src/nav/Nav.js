import React from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	AsyncStorage,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Button } from 'react-native-elements';

import Intro from '../screens/intro/Intro';
import Bot from '../screens/bot/Bot';
import Home from '../screens/home/Home';
import Claims from '../screens/claims/Claims';
import Claim from '../screens/claim/Claim';
import TicketStack from './TicketStack';
import menu from '../../assets/images/menu.png';
import styles from './styles';

const reset = () => {
	AsyncStorage.getAllKeys((error, keys) => {
		console.log('getAllKeys', keys);
		AsyncStorage.multiRemove(keys, (errors) => {
			console.log('multiRemove errors', errors);
		});
	});
};

export default createStackNavigator({
	Intro: { screen: Intro, navigationOptions: { header: null } },
	Bot: {
		screen: Bot,
		navigationOptions: {
			headerLeft: (
				<TouchableOpacity onPress={() => {}} style={styles.headerLeftContainer}>
					<Image
						source={menu}
						style={styles.headerLeftIcon}
						resizeMode="contain"
					/>
				</TouchableOpacity>
			),
			headerTitle: (
				<View style={styles.titleContainerStyle}>
					<Text style={styles.titleTextStyle}>APP</Text>
				</View>
			),
			headerStyle: styles.headerContainerStyle,
			headerRight: (
				<Button
					title="Reset"
					titleStyle={styles.buttonTitleStyle}
					onPress={reset}
					clear
				/>
			),
		},
	},
	Home: { screen: Home, navigationOptions: { header: null } },
	Claims: { screen: Claims, navigationOptions: { header: null } },
	Claim: { screen: Claim, navigationOptions: { header: null } },
	TicketStack: {
		screen: TicketStack,
		navigationOptions: { header: null, gesturesEnabled: false },
	},
});
