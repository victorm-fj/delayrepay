// import { StyleSheet, Platform } from 'react-native';

// export default StyleSheet.create({
// 	container: {
// 		flex: 1,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		backgroundColor: '#F5FCFF',
// 	},
// 	scannerContainer: {
// 		flex: 1,
// 		width: '100%',
// 		alignItems: 'center',
// 	},
// 	scanner: {
// 		// width: 250,
// 		// height: 400,
// 		width: '100%',
// 		height: '100%',
// 	},
// 	loaderContainer: {
// 		flex: 1,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 	},
// 	modal: {
// 		justifyContent: 'space-around',
// 		alignItems: 'center',
// 		backgroundColor: 'lightblue',
// 	},
// 	modalImage: {
// 		width: '70%',
// 		height: '60%',
// 	},
// 	modalTextContainer: {
// 		flexDirection: 'row',
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 	},
// 	modalActionsContainer: {
// 		flexDirection: 'row',
// 		width: '100%',
// 		alignItems: 'center',
// 		justifyContent: 'space-around',
// 	},
// 	modelTopLeftMark: {
// 		width: 50,
// 		height: 50,
// 		borderTopLeftRadius: 20,
// 		borderLeftWidth: 10,
// 		borderTopWidth: 10,
// 		position: 'absolute',
// 		top: -7,
// 		left: -7,
// 	},
// 	modelTopRightMark: {
// 		width: 50,
// 		height: 50,
// 		borderTopRightRadius: 20,
// 		borderTopWidth: 10,
// 		borderRightWidth: 10,
// 		position: 'absolute',
// 		top: -7,
// 		right: -7,
// 	},
// 	modelBottomLeftMark: {
// 		width: 50,
// 		height: 50,
// 		borderBottomLeftRadius: 20,
// 		borderLeftWidth: 10,
// 		borderBottomWidth: 10,
// 		position: 'absolute',
// 		bottom: -7,
// 		left: -7,
// 	},
// 	modelBottomRightMark: {
// 		width: 50,
// 		height: 50,
// 		borderBottomRightRadius: 20,
// 		borderBottomWidth: 10,
// 		borderRightWidth: 10,
// 		position: 'absolute',
// 		bottom: -7,
// 		right: -7,
// 	},
// 	roundedShadowBox: {
// 		borderRadius: 6,
// 		...Platform.select({
// 			ios: {
// 				shadowColor: 'rgba(0,0,0,0.2)',
// 				shadowOpacity: 1,
// 				shadowRadius: 3,
// 				shadowOffset: {
// 					width: 2,
// 					height: 4,
// 				},
// 			},
// 			android: {
// 				elevation: 6,
// 			},
// 		}),
// 	},
// 	ticketRoundedShadow: {
// 		borderRadius: 6,
// 		...Platform.select({
// 			ios: {
// 				shadowColor: 'rgba(0,0,0,0.1)',
// 				shadowOpacity: 1,
// 				shadowRadius: 3,
// 				shadowOffset: {
// 					width: 2,
// 					height: 4,
// 				},
// 			},
// 			android: {
// 				elevation: 6,
// 			},
// 		}),
// 	},
// });

import { StyleSheet } from 'react-native';
import { iOSColors } from 'react-native-typography';

const colors = {
	white: '#f9fafc',
	black: '#3b4859',
	gray: '#8392a7',
	purple: iOSColors.purple,
};

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
		backgroundColor: colors.white,
	},
});
