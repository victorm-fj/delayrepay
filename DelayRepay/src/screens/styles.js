import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	scannerContainer: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
	},
	scanner: {
		width: '100%',
		height: '100%',
	},
	modelTopLeftMark: {
		width: 50,
		height: 50,
		borderTopLeftRadius: 20,
		borderLeftWidth: 10,
		borderTopWidth: 10,
		position: 'absolute',
		top: -7,
		left: -7,
	},
	modelTopRightMark: {
		width: 50,
		height: 50,
		borderTopRightRadius: 20,
		borderTopWidth: 10,
		borderRightWidth: 10,
		position: 'absolute',
		top: -7,
		right: -7,
	},
	modelBottomLeftMark: {
		width: 50,
		height: 50,
		borderBottomLeftRadius: 20,
		borderLeftWidth: 10,
		borderBottomWidth: 10,
		position: 'absolute',
		bottom: -7,
		left: -7,
	},
	modelBottomRightMark: {
		width: 50,
		height: 50,
		borderBottomRightRadius: 20,
		borderBottomWidth: 10,
		borderRightWidth: 10,
		position: 'absolute',
		bottom: -7,
		right: -7,
	},
	loaderContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	mainContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: 20,
		width: '100%',
	},
	modalImage: {
		width: '85%',
		height: '100%',
	},
	actionsContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonContainerStyle: {
		marginBottom: 40,
		minWidth: 230,
	},
	buttonStyle: {
		paddingVertical: 3,
		paddingHorizontal: 67,
		backgroundColor: '#3b7cff',
		borderRadius: 30,
	},
	buttonTitleStyle: {
		// fontFamily: 'Gotham-Book',
		fontSize: 16,
		color: '#ffffff',
		letterSpacing: 1,
	},
	docScannerContainer: {
		...Platform.select({
			ios: {
				width: width * 0.7,
				height: height * 0.6,
			},
			android: {
				width: height * 0.6,
				height: width * 0.7,
			},
		}),
	},
});
