import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { sanFranciscoSpacing } from 'react-native-typography';

class Claim extends Component {
	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: 'rgba(250,250,250,1)',
				}}
			>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
						paddingHorizontal: 20,
						marginTop: 55,
						width: '100%',
						alignItems: 'center',
					}}
				>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<Ionicons name="ios-close" size={32} />
					</TouchableOpacity>
					<Text
						style={{
							fontFamily: 'SF Pro Text',
							fontSize: 17,
							lineHeight: 20,
							letterSpacing: sanFranciscoSpacing(-0.41),
							color: 'rgba(40,60,80,1)',
						}}
					>
						Claim
					</Text>
					<TouchableOpacity onPress={() => {}}>
						<Ionicons name="ios-contact" size={28} />
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

export default Claim;
