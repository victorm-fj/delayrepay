import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import { human } from 'react-native-typography';
import { Button } from 'react-native-elements';

import { ScrollView } from 'react-native-gesture-handler';
import styles from '../styles';

class TextDetection extends Component {
	renderTextResult() {
		const { navigation } = this.props;
		const result = navigation.getParam('result', null);
		if (result !== null) {
			if (Platform.OS === 'android') {
				return result.map((textValue) => (
					<Text
						key={textValue}
						style={[human.caption1, { fontWeight: 'bold' }]}
					>
						Text: {textValue} |
					</Text>
				));
			}
			return result.TextDetections.map((detection) => (
				<View key={`${detection.Id}`} style={styles.modalTextContainer}>
					<Text style={[human.caption1, { fontWeight: 'bold' }]}>
						Text: {detection.DetectedText} |
					</Text>
					<Text style={human.caption1}>
						| Confidence: {detection.Confidence.toFixed(2)}
					</Text>
				</View>
			));
		}
		return null;
	}

	render() {
		const { navigation } = this.props;
		return (
			<View style={styles.container}>
				<ScrollView
					style={{
						flex: 1,
						paddingHorizontal: 20,
						marginVertical: 20,
						width: '100%',
					}}
					contentContainerStyle={{
						flexGrow: 1,
						justifyContent: 'center',
						alignItems: 'center',
						paddingBottom: 44,
					}}
				>
					{this.renderTextResult()}
				</ScrollView>
				<View style={styles.actionsContainer}>
					<Button
						title="Confirm"
						onPress={() => navigation.navigate('Bot', { continue: true })}
						titleStyle={styles.buttonTitleStyle}
						buttonStyle={styles.buttonStyle}
						containerStyle={styles.buttonContainerStyle}
					/>
				</View>
			</View>
		);
	}
}

export default TextDetection;
