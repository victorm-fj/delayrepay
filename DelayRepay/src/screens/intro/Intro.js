// @flow
import * as React from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';

import Loader from '../../components/Loader';
import Slider from '../../components/slider/Slider';
import slide1 from '../../../assets/images/slide1.png';
import slide2 from '../../../assets/images/slide2.png';
import slide3 from '../../../assets/images/slide3.png';
import styles from './styles';

const asyncStorageKey = '@delayRepayScreen:intro';
const slides = [
	{
		id: 0,
		image: slide1,
		text: 'Easy way to get automatically compensated for train delays',
		big: true,
	},
	{
		id: 1,
		image: slide2,
		text: 'Scan your tickets and we\'ll refund you for your delays',
		big: true,
	},
	{
		id: 2,
		image: slide3,
		text: 'Money could be sitting pretty in your bank account already',
		big: true,
	},
];

type Props = {
	navigation: NavigationScreenProp<*>,
	screenProps: NavigationScreenProp<*>,
};
type State = { loading: boolean };
class Intro extends React.Component<Props, State> {
	state = { loading: true };

	async componentDidMount() {
		const { navigation } = this.props;
		try {
			const introValue = await AsyncStorage.getItem(asyncStorageKey);
			if (introValue) {
				return navigation.replace('Bot');
			}
			await AsyncStorage.setItem(asyncStorageKey, 'value');
		} catch (error) {
			console.log('AsyncStorage error:', error);
		}
		return this.setState({ loading: false });
	}

	render() {
		const { loading } = this.state;
		const { navigation } = this.props;
		if (loading) {
			return <Loader />;
		}
		return (
			<View style={styles.introContainer}>
				<Slider slides={slides} />
				<View style={styles.actionsContainer}>
					<Button
						title="Get started"
						onPress={() => navigation.replace('Bot')}
						titleStyle={styles.buttonTitleStyle}
						buttonStyle={styles.buttonStyle}
						containerStyle={styles.buttonContainerStyle}
					/>
					<Text style={styles.footnoteTextStyle}>
						By continuing you agree to our{' '}
						<Text style={styles.footnoteActionTextStyle}>Terms of use</Text>.
					</Text>
				</View>
			</View>
		);
	}
}

export default Intro;
