import React from 'react';

import PhoneNumber from './phoneNumber/PhoneNumber';
import AuthContainer from './authComponent/AuthContainer';
import ConfirmCodeContainer from './confirmCode/ConfirmCodeContainer';
import TicketScan from './ticketScan/TicketScan';
import UploadSuccess from './uploadSuccess/UploadSuccess';

export default [
	{
		id: '0',
		message:
			'I\'m App, the app that\'s going to help you get refunds when your trains are delayed for 15 mins or more without you needing to lift a finger.',
		trigger: '1',
		// trigger: 'uploadSuccess',
	},
	{
		id: '1',
		message: 'What\'s your name?',
		trigger: 'name',
	},
	{
		id: 'name',
		user: true,
		trigger: '2',
		// trigger: 'getStarted',
	},
	{
		id: '2',
		message: ({ previousValue }) =>
			`Hi ${previousValue.trim()}. Nice to meet you [emojiName="wave"]`,
		trigger: '3',
	},
	{
		id: '3',
		message:
			'Can I have your mobile number? I know we just met but I need to check you\'re a real person before I get you set up. [emojiName="blush"]',
		trigger: 'registerOptions',
	},
	{
		id: 'registerOptions',
		options: [
			{
				value: 'yes',
				label: 'Yes, sure',
				highlighted: true,
				trigger: 'phoneNumber1',
			},
			{ value: 'no', label: 'Nope, not yet', trigger: 'end' },
		],
	},
	{
		id: 'phoneNumber1',
		user: true,
		trigger: 'phoneCustom1',
	},
	{
		id: 'phoneCustom1',
		component: <PhoneNumber id="1" />,
		waitAction: true,
		replace: true,
	},
	{
		id: 'invalidPhone1',
		message: 'I don\'t think that number\'s correct. Can you please try again?',
		trigger: 'phoneNumber1',
	},
	{
		id: 'authenticate',
		component: <AuthContainer />,
		waitAction: true,
		replace: true,
	},
	{
		id: 'authError',
		message: 'Something went wrong, please try again.',
		trigger: 'phoneNumber1',
	},
	// / -- USER has begun authentication --- ///
	{
		id: '4',
		message:
			'I\'ve sent you a 6-digit code. Enter it below and we\'re good to go.',
		trigger: 'confirmCode1',
	},
	{
		id: 'confirmCode1',
		user: true,
		trigger: 'codeCustom1',
	},
	{
		id: 'codeCustom1',
		component: <ConfirmCodeContainer id="1" />,
		waitAction: true,
		replace: true,
	},
	{
		id: 'invalidCode1',
		message: 'Nope. That\'s not the right code. Can you try again?',
		trigger: 'confirmCode2',
	},
	{
		id: 'confirmCode2',
		user: true,
		trigger: 'codeCustom2',
	},
	{
		id: 'codeCustom2',
		component: <ConfirmCodeContainer id="2" />,
		waitAction: true,
		replace: true,
	},
	{
		id: 'changePhoneOptions',
		options: [
			{
				value: 'changePhone',
				label: 'Change my number',
				trigger: 'updatePhone',
				highlighted: true,
			},
		],
	},
	{
		id: 'updatePhone',
		message: 'Please enter a new number',
		trigger: 'phoneNumber1',
	},
	{
		id: 'invalidCodeOptions',
		options: [
			{
				value: 'get a new code',
				label: 'Get a new code',
				trigger: 'authenticate',
				highlighted: true,
			},
			{
				value: 'change my number',
				label: 'Change my number',
				trigger: 'updatePhone',
			},
		],
	},
	// / --- USER has been authenticated --- ///
	{
		id: 'getStarted',
		message: 'Great!, Let\'s get started. [emojiName="+1"]',
		trigger: 'askForTicket',
	},
	{
		id: 'askForTicket',
		message:
			'To automatically claim refunds for your train delays, I\'ll need your train tickets.\nDo you have a train ticket I could see?',
		trigger: 'uploadTicketOptions',
	},
	{
		id: 'uploadTicketOptions',
		options: [
			{
				value: 'yes',
				label: 'Yes, sure',
				highlighted: true,
				trigger: 'ticketScan',
			},
			{ value: 'no', label: 'Nope, not yet', trigger: 'end' },
		],
	},
	{
		id: 'ticketScan',
		component: <TicketScan />,
		waitAction: true,
		replace: true,
	},
	{
		id: 'uploadSuccess',
		component: <UploadSuccess />,
		trigger: 'successMessage',
	},
	{
		id: 'successMessage',
		message:
			'I\'ll notify you when I\'m able to claim compensation for your train delays.',
		trigger: 'quickOptions',
	},
	{
		id: 'quickOptions',
		options: [
			{ value: 'claims', label: 'Claims', highlighted: true, trigger: 'end' },
			{ value: 'tickets', label: 'Tickets', trigger: 'end' },
		],
	},
	{
		id: 'end',
		message: 'End of conversation',
		end: true,
	},
];
