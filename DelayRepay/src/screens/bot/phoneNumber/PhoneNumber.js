// @flow
import React from 'react';
import { parseNumber, formatNumber, isValidNumber } from 'libphonenumber-js';

import { Loading } from '../../../components/chatbot';

type Props = {
	triggerNextStep: Function,
	steps: Object,
	id: string,
};
class PhoneNumber extends React.Component<Props> {
	async componentDidMount() {
		const { triggerNextStep, steps, id } = this.props;
		const countryIso2 = 'GB';
		// const countryIso2 = 'MX';
		// Get phoneNumber from the right step
		const phoneNumber = steps[`phoneNumber${id}`].value.trim();
		// Parse the phoneNumber with the deviceCountry
		const parsedNumber = parseNumber(phoneNumber, countryIso2);
		if (parsedNumber.phone && isValidNumber(parsedNumber)) {
			// Phone number is a valid phone number, then continue to signup/signin
			const formattedNumber = formatNumber(parsedNumber, 'E.164');
			return triggerNextStep({
				value: formattedNumber, // update value to international 'E.164' format
				trigger: 'authenticate',
				self: true, // reset value to undefined
			});
		}
		return triggerNextStep({
			value: undefined,
			trigger: 'invalidPhone1',
			self: true, // reset value to undefined
		});
	}

	render() {
		return <Loading />;
	}
}

export default PhoneNumber;
