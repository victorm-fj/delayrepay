// @flow
import React from 'react';
import { Provider } from 'unstated';

import ChatBot from '../../components/chatbot';
import steps from './steps';

type Props = {};
type State = { botKey: number, cognitoUser: any };
class Bot extends React.Component<Props, State> {
	state = { botKey: 0 };

	render() {
		const { botKey } = this.state;
		return (
			<Provider>
				<ChatBot
					key={botKey}
					cache
					cacheName="@delayRepayBot"
					style={{ backgroundColor: 'transparent' }}
					steps={steps}
					inputMetadata={{
						name: { keyboardType: 'default', placeholder: 'Type your name' },
						phoneNumber1: {
							keyboardType: 'numeric',
							placeholder: 'Enter your phone number',
						},
						phoneNumber2: {
							keyboardType: 'numeric',
							placeholder: 'Enter your phone number',
						},
						confirmCode1: {
							keyboardType: 'numeric',
							placeholder: 'Enter the code',
						},
						confirmCode2: {
							keyboardType: 'numeric',
							placeholder: 'Enter the code',
						},
					}}
					resetConditions={{
						trigger: 'confirmCode',
						checkStepId: 'authenticate',
						conditionValue: 'registeredUser',
						asyncStorageKey: '@delayRepayBot:authStepValue',
					}}
					displayChat={['confirmCode1', 'confirmCode2']}
					handleChat={{
						confirmCode1: { trigger: 'changePhoneOptions' },
						confirmCode2: { trigger: 'invalidCodeOptions' },
					}}
					showOptions={{
						codeCustom2: {
							stepCheckPoint: '4',
							allowedAttempts: 3,
							invalidStepId: 'invalidCode1',
						},
					}}
				/>
			</Provider>
		);
	}
}

export default Bot;
