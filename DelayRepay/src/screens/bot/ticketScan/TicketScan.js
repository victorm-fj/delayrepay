// @flow
import React from 'react';
import { withNavigation } from 'react-navigation';
import type { NavigationScreenProp } from 'react-navigation';

import { Loading } from '../../../components/chatbot';

type Props = {
	navigation: NavigationScreenProp<*>,
	triggerNextStep: Function,
	steps: Object,
};
class TicketScan extends React.Component<Props> {
	componentDidMount() {
		console.log('TicketScan component mounted');
		const { navigation } = this.props;
		navigation.navigate('RecognitionScan');
	}

	componentDidUpdate() {
		console.log('TicketScan component updated');
		const { navigation, triggerNextStep } = this.props;
		const continueParam = navigation.getParam('continue', true);
		if (continueParam) {
			triggerNextStep({ trigger: 'uploadSuccess' });
		}
	}

	render() {
		return <Loading />;
	}
}

export default withNavigation(TicketScan);
