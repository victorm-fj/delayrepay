// @flow
import React from 'react';
import { AsyncStorage } from 'react-native';
import { Auth } from 'aws-amplify';

import { Loading } from '../../../components/chatbot';

type Props = {
	id: string,
	cognitoUser: any,
	triggerNextStep: Function,
	steps: Object,
};
class ConfirmCode extends React.Component<Props> {
	async componentDidMount() {
		const { steps, triggerNextStep, cognitoUser, id } = this.props;
		// Get all needed info from previous step
		const code = steps[`confirmCode${id}`].value.trim();
		const isCodeValid = code.length === 6 && !Number.isNaN(Number(code));
		// Code is a valid code
		if (isCodeValid) {
			try {
				const stringifiedAuthStepValue = await AsyncStorage.getItem(
					'@delayRepayBot:authStepValue'
				);
				const authStepValue = JSON.parse(stringifiedAuthStepValue);
				if (authStepValue === 'registeredUser' && cognitoUser) {
					await Auth.sendCustomChallengeAnswer(cognitoUser, code);
					const givenName = await this.getCurrentUserGivenName();
					console.log('/// Given name ///', givenName);
					if (givenName) {
						return triggerNextStep({
							trigger: 'getStarted',
							update: { stepId: 'name', value: givenName },
						});
					}
					return triggerNextStep({ trigger: 'getStarted' });
				}
				await Auth.confirmSignUp(authStepValue.username, code);
				await Auth.signIn(authStepValue.username, authStepValue.password);
				await AsyncStorage.removeItem('@delayRepayBot:authStepValue');
				const givenName = await this.getCurrentUserGivenName();
				console.log('/// Given name ///', givenName);
				if (givenName) {
					return triggerNextStep({
						trigger: 'getStarted',
						update: { stepId: 'name', value: givenName },
					});
				}
				return triggerNextStep({ trigger: 'getStarted' });
			} catch (error) {
				console.log('ConfirmCode error:', error);
				return triggerNextStep({
					trigger: 'invalidCode1',
					value: undefined,
					self: true,
				});
			}
		}
		return triggerNextStep({
			trigger: 'invalidCode1',
			value: undefined,
			self: true,
		});
	}

	getCurrentUserGivenName = async (): string => {
		let givenName = '';
		try {
			const user = await Auth.currentAuthenticatedUser();
			const attributes = await Auth.userAttributes(user);
			const { Value } = attributes.filter(
				(attr) => attr.Name === 'given_name'
			)[0];
			givenName = Value;
		} catch (error) {
			console.log('getCurrentUserGivenName error:', error);
		}
		return givenName;
	};

	render() {
		return <Loading />;
	}
}

export default ConfirmCode;
