// @flow
import React from 'react';
import { Subscribe } from 'unstated';

import { UnstatedBot } from '../UnstatedBot';
import ConfirmCode from './ConfirmCode';

const ConfirmCodeContainer = (props: Object) => (
	<Subscribe to={[UnstatedBot]}>
		{(botStore) => (
			<ConfirmCode {...props} cognitoUser={botStore.state.cognitoUser} />
		)}
	</Subscribe>
);

export default ConfirmCodeContainer;
