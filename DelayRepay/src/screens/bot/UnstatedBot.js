import { Container } from 'unstated';

class UnstatedBot extends Container {
	state = { cognitoUser: null };

	setCognitoUser = (cognitoUser) => {
		this.setState({ cognitoUser });
	};
}

export { UnstatedBot };
