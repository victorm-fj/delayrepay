// @flow
import React from 'react';
import { AsyncStorage } from 'react-native';
import { Auth } from 'aws-amplify';
import uuidV4 from 'uuid/v4';

import { Loading } from '../../../components/chatbot';

type Props = {
	setCognitoUser: Function,
	triggerNextStep: Function,
	steps: Object,
};
class AuthComponent extends React.Component<Props> {
	componentDidMount() {
		const { steps } = this.props;
		// getNewCode is defined if user has chosen 'Get a new code' from invalidCodeOptions step
		// its value is undefined if user has not selected this option
		const getNewCode = steps.invalidCodeOptions;
		if (
			getNewCode &&
			getNewCode.value &&
			getNewCode.value === 'get a new code'
		) {
			return this.getNewCode();
		}
		// Try registering the user as a new user
		return this.signUp();
	}

	getNewCode = async () => {
		const { triggerNextStep } = this.props;
		try {
			const stringifiedAuthStepValue = await AsyncStorage.getItem(
				'@delayRepayBot:authStepValue'
			);
			const authStepValue = JSON.parse(stringifiedAuthStepValue);
			if (authStepValue === 'registeredUser') {
				return this.signIn();
			}
			await Auth.resendSignUp(authStepValue.username);
			return triggerNextStep({
				trigger: '4',
				update: { stepId: 'invalidCodeOptions', value: undefined },
			});
		} catch (error) {
			console.log('getNewCode error:', error);
			return triggerNextStep({ trigger: 'authError' });
		}
	};

	signUp = async () => {
		const { steps, triggerNextStep } = this.props;
		// Get all needed info from previous steps
		const phoneNumber = steps.phoneNumber1.value || steps.phoneNumber2.value;
		const givenName = steps.name.value;
		const username = uuidV4();
		const password = `DelayRepay-${uuidV4().slice(0, 8)}`;
		try {
			await Auth.signUp({
				username,
				password,
				attributes: { phone_number: phoneNumber, given_name: givenName },
			});
			const authStepValue = JSON.stringify({ username, password });
			await AsyncStorage.setItem('@delayRepayBot:authStepValue', authStepValue);
			return triggerNextStep({ trigger: '4' });
		} catch (error) {
			console.log('signUp error:', error);
			if (
				error.code === 'UsernameExistsException' ||
				error.code === 'UserLambdaValidationException'
			) {
				return this.signIn();
			}
			return triggerNextStep({ trigger: 'authError' });
		}
	};

	signIn = async () => {
		const { steps, triggerNextStep, setCognitoUser } = this.props;
		// Get all needed info from previous steps
		const phoneNumber = steps.phoneNumber1.value || steps.phoneNumber2.value;
		try {
			const cognitoUser = await Auth.signIn(phoneNumber);
			setCognitoUser(cognitoUser);
			const authStepValue = JSON.stringify('registeredUser');
			await AsyncStorage.setItem('@delayRepayBot:authStepValue', authStepValue);
			return triggerNextStep({ trigger: '4' });
		} catch (error) {
			console.log('signIn error:', error);
			return triggerNextStep({ trigger: 'authError' });
		}
	};

	render() {
		return <Loading />;
	}
}

export default AuthComponent;
