// @flow
import React from 'react';
import { Subscribe } from 'unstated';

import { UnstatedBot } from '../UnstatedBot';
import AuthComponent from './AuthComponent';

const AuthContainer = (props: Object) => (
	<Subscribe to={[UnstatedBot]}>
		{(botStore) => (
			<AuthComponent {...props} setCognitoUser={botStore.setCognitoUser} />
		)}
	</Subscribe>
);

export default AuthContainer;
