// @flow
import React from 'react';
import { View, Text } from 'react-native';
import Image from 'react-native-image-progress';

import { Loading } from '../../../components/chatbot';

const obamaClapping = require('../../../../assets/images/obama_clapping.gif');

type Props = { steps: Object, triggerNextStep: Function };
type State = { loading: boolean };
class UploadSuccess extends React.Component<Props, State> {
	state = { loading: true };

	componentDidMount() {
		const { triggerNextStep } = this.props;
		setTimeout(
			() =>
				this.setState({ loading: false }, () =>
					triggerNextStep({ trigger: 'successMessage' })
				),
			1000
		);
	}

	render() {
		const { loading } = this.state;
		const {
			steps: { name },
		} = this.props;
		if (loading) {
			return <Loading />;
		}
		return (
			<View
				style={{
					flex: 1,
					borderRadius: 20,
					overflow: 'hidden',
					marginVertical: -5,
					marginLeft: -10,
					marginRight: -10,
					paddingBottom: 5,
				}}
			>
				<Image
					source={obamaClapping}
					resizeMode="cover"
					style={{
						width: 230,
						height: 170,
					}}
					imageStyle={{
						width: 230,
						height: 170,
					}}
				/>
				<Text
					style={{
						paddingTop: 7,
						paddingLeft: 7,
						fontSize: 14,
						fontFamily: 'GothamRounded-Book',
						lineHeight: 18,
						color: '#3b4859',
					}}
				>
					Well done {name.value ? name.value : ''}!
				</Text>
			</View>
		);
	}
}

export default UploadSuccess;
