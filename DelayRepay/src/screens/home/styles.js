import { StyleSheet } from 'react-native';
import { iOSColors, sanFranciscoSpacing } from 'react-native-typography';

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: 'rgba(250,250,250,1)',
		paddingTop: 20,
	},
	carouselItemContainer: {
		backgroundColor: iOSColors.white,
		width: '90%',
		height: 200,
	},
	carouselSlideStyle: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	carouselItemContentContainer: {
		flex: 1,
		borderRadius: 8,
		overflow: 'hidden',
	},
	carouselItemImage: {
		height: 125,
		width: '100%',
	},
	stackedCards: {
		position: 'absolute',
		top: 0,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	listHeaderContainer: {
		flexDirection: 'row',
		paddingHorizontal: 20,
		paddingBottom: 16,
		paddingTop: 26,
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	listHeaderTitle: {
		fontFamily: 'SF Pro Text',
		fontSize: 17,
		lineHeight: 22,
		letterSpacing: sanFranciscoSpacing(-0.41),
		color: 'rgba(40,60,80,1)',
	},
	listHeaderActionText: {
		fontFamily: 'SF Pro Text',
		fontSize: 12,
		lineHeight: 14,
		letterSpacing: sanFranciscoSpacing(-0.13),
		color: 'rgba(40,60,80,1)',
	},
});
