import React, { Component } from 'react';
import {
	View,
	FlatList,
	Dimensions,
	Text,
	TouchableOpacity,
	Animated,
	Easing,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { PanGestureHandler, State } from 'react-native-gesture-handler';

import ClaimItem from './ClaimItem';
import CarouselItem from './CarouselItem';
import { ticketsData, claimsData } from './mockData';
import styles from '../../styles';
import homeStyles from './styles';

const { width, height } = Dimensions.get('window');
const TOP_SECTION_HEIGHT = 385;

class Home extends Component {
	constructor(props) {
		super(props);
		this.translateY = new Animated.Value(0);
		this.lastOffset = { y: 0 };
		this.onGestureEvent = Animated.event(
			[
				{
					nativeEvent: {
						translationY: this.translateY,
					},
				},
			],
			{ useNativeDriver: true }
		);
	}

	onHandlerStateChange = (event) => {
		if (event.nativeEvent.oldState === State.ACTIVE) {
			this.lastOffset.y += event.nativeEvent.translationY;
			this.translateY.setOffset(this.lastOffset.y);
			this.translateY.setValue(0);
		}
	};

	onShowAllHandler = () => {
		Animated.timing(this.translateY, {
			toValue: -TOP_SECTION_HEIGHT,
			duration: 500,
			useNativeDriver: true,
			easing: Easing.linear,
		}).start(() => {
			this.lastOffset.y = 0;
			this.lastOffset.y += -TOP_SECTION_HEIGHT;
			this.translateY.setOffset(this.lastOffset.y);
			this.translateY.setValue(0);
		});
	};

	renderCarouselItem = ({ item, index }) => {
		const shadow =
			ticketsData.length === 1
				? styles.roundedShadowBox
				: styles.ticketRoundedShadow;
		return <CarouselItem key={`${item.id}-${index}`} shadow={shadow} />;
	};

	renderItem = ({ item }) => (
		<TouchableOpacity onPress={() => this.props.navigation.navigate('Claim')}>
			<ClaimItem key={item.id} {...item} />
		</TouchableOpacity>
	);

	render() {
		const animStyles = {
			transform: [
				{
					translateY: this.translateY.interpolate({
						inputRange: [-TOP_SECTION_HEIGHT, 0, TOP_SECTION_HEIGHT],
						outputRange: [-TOP_SECTION_HEIGHT, 0, 0],
						extrapolate: 'clamp',
					}),
				},
			],
		};
		return (
			<View style={homeStyles.screenContainer}>
				<View
					style={{
						position: 'absolute',
						top: 285,
						backgroundColor: 'white',
						width: '100%',
						height: '100%',
					}}
				/>
				<View
					style={[
						{
							height: TOP_SECTION_HEIGHT,
							width: '100%',
							justifyContent: 'center',
							alignItems: 'center',
						},
					]}
				>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							paddingHorizontal: 20,
							marginTop: 35,
							width: '100%',
						}}
					>
						<TouchableOpacity
							onPress={() => this.props.navigation.navigate('Claims')}
						>
							<Ionicons name="ios-menu" size={28} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {}}>
							<Ionicons name="ios-contact" size={28} />
						</TouchableOpacity>
					</View>
					<View
						style={{
							flexDirection: 'row',
							marginTop: 45,
							marginBottom: 28,
							paddingHorizontal: 20,
							alignItems: 'center',
						}}
					>
						<Text style={[{ flex: 1 }, homeStyles.listHeaderTitle]}>
							Ticket
						</Text>
						<TouchableOpacity
							onPress={() => this.props.navigation.navigate('Recognition')}
						>
							<Text style={homeStyles.listHeaderActionText}>Add new</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.onShowAllHandler}>
							<Text
								style={[homeStyles.listHeaderActionText, { marginLeft: 13 }]}
							>
								View all
							</Text>
						</TouchableOpacity>
					</View>

					<Carousel
						loop
						data={ticketsData}
						renderItem={this.renderCarouselItem}
						sliderWidth={width}
						sliderHeight={170}
						itemWidth={width - 40}
						itemHeight={170}
						layout="tinder"
						removeClippedSubviews={false}
					/>
				</View>
				<PanGestureHandler
					onGestureEvent={this.onGestureEvent}
					onHandlerStateChange={this.onHandlerStateChange}
				>
					<Animated.View style={animStyles}>
						<View
							style={{
								position: 'absolute',
								top: 0,
								backgroundColor: 'white',
								width: '100%',
								height,
							}}
						/>
						<View style={homeStyles.listHeaderContainer}>
							<Text style={homeStyles.listHeaderTitle}>Possible claims</Text>
							<TouchableOpacity onPress={this.onShowAllHandler}>
								<Text style={homeStyles.listHeaderActionText}>View all</Text>
							</TouchableOpacity>
						</View>
						<FlatList
							contentContainerStyle={{
								flexGrow: 1,
								paddingBottom: 20,
								backgroundColor: 'white',
							}}
							data={claimsData}
							extraData={claimsData}
							keyExtractor={(item, index) => `${item.id}-${index}`}
							renderItem={this.renderItem}
						/>
					</Animated.View>
				</PanGestureHandler>
			</View>
		);
	}
}

export default Home;
