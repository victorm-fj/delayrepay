import React from 'react';
import { View, Text } from 'react-native';
import { sanFranciscoSpacing } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Image from 'react-native-remote-svg';

import bitmap from './Bitmap.svg';
import blueRectangle from './BlueRectangle.svg';
import fromTo from './fromto.svg';

const CarouselItem = ({ shadow }) => (
	<View
		style={[
			{
				padding: 30,
				height: 170,
				width: '100%',
				backgroundColor: 'rgba(255,255,255,1)',
			},
			shadow,
		]}
	>
		<View
			style={{
				flexDirection: 'row',
				justifyContent: 'space-between',
				alignItems: 'center',
				marginBottom: 30,
			}}
		>
			<View
				style={{
					width: 170,
					height: 26,
					paddingVertical: 5,
					paddingLeft: 30,
					paddingRight: 13,
					marginLeft: -30,
				}}
			>
				<Image source={blueRectangle} style={{ position: 'absolute' }} />
				<Text
					style={{
						color: 'rgba(255,255,255,1)',
						fontFamily: 'SF Pro Text',
						fontSize: 12,
						lineHeight: 16,
						letterSpacing: sanFranciscoSpacing(-0.13),
					}}
				>
					Valid until 12/06/18
				</Text>
			</View>
			<Text
				style={{
					color: 'rgba(0,122,255,1)',
					fontFamily: 'SF Pro Text',
					fontSize: 19,
					lineHeight: 24,
					letterSpacing: sanFranciscoSpacing(-0.46),
				}}
			>
				&pound;179.50
			</Text>
		</View>
		<View
			style={{
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}
		>
			<View>
				<View
					style={{
						flexDirection: 'row',
						alignItems: 'center',
						marginBottom: 26,
					}}
				>
					<Ionicons
						name="ios-pin"
						size={16}
						color="white"
						style={{ marginLeft: 1, marginRight: 10 }}
					/>
					<Text
						style={{
							color: 'rgba(40,60,80,1)',
							fontFamily: 'SF Pro Text',
							fontSize: 14,
							lineHeight: 16,
							letterSpacing: sanFranciscoSpacing(-0.15),
						}}
					>
						Birmingham New Street
					</Text>
				</View>
				<View style={{ flexDirection: 'row', alignItems: 'center' }}>
					<Ionicons
						name="ios-time"
						size={14}
						style={{ marginRight: 10 }}
						color="white"
					/>
					<Image source={fromTo} style={{ position: 'absolute', top: -21 }} />
					<Text
						style={{
							color: 'rgba(40,60,80,1)',
							fontFamily: 'SF Pro Text',
							fontSize: 14,
							lineHeight: 16,
							letterSpacing: sanFranciscoSpacing(-0.15),
						}}
					>
						Birmingham New Street
					</Text>
				</View>
			</View>
			<Image source={bitmap} />
		</View>
	</View>
);

export default CarouselItem;
