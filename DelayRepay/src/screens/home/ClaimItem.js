import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { iOSColors, sanFranciscoSpacing } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Image from 'react-native-remote-svg';

import styles from '../../styles';
import location from './Location.svg';
import delay from './Delay.svg';
import dots from './Dots.svg';

const itemStyles = StyleSheet.create({
	claimItemContainer: {
		backgroundColor: iOSColors.white,
		marginVertical: 10,
		marginHorizontal: 20,
		height: 96,
	},
	claimItemRow: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
	claimItemContent: {
		flex: 1,
		borderRadius: 6,
		overflow: 'hidden',
		justifyContent: 'space-between',
	},
	claimItemMainContent: {
		paddingTop: 14,
		paddingBottom: 12,
		paddingHorizontal: 15,
		height: 72,
	},
	claimItemDepartureRow: {
		paddingBottom: 14,
		flexDirection: 'row',
		alignItems: 'center',
	},
	claimItemArrivalRow: {
		paddingBottom: 12,
		flexDirection: 'row',
		alignItems: 'center',
	},
	claimMainText: {
		fontSize: 14,
		lineHeight: 16,
		letterSpacing: sanFranciscoSpacing(-0.15),
	},
	claimGrayLabel: {
		fontSize: 12,
		lineHeight: 16,
		letterSpacing: sanFranciscoSpacing(-0.13),
		color: '#a3abb4',
	},
	claimItemFooter: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 15,
		height: 24,
	},
	claimItemStatus: {
		fontSize: 12,
		lineHeight: 16,
		letterSpacing: sanFranciscoSpacing(-0.13),
	},
	claimItemPounds: {
		fontSize: 17,
		lineHeight: 24,
		letterSpacing: sanFranciscoSpacing(-0.41),
	},
});

const lightRed = '#ff3b30';
const lightGreen = '#4cd964';
const lightBlue = '#fafafa';
const orange = '#ffb54b';
const gray = '#a3abb4';
const blue = '#057cfe';

const getStatusColor = (status) => {
	let color = lightBlue;
	if (status === 'Refunded') {
		color = lightGreen;
	} else if (status === 'Rejected') {
		color = lightRed;
	}
	return color;
};

const ClaimItem = ({
	date,
	duration,
	from,
	departureTime,
	to,
	arrivalTime,
	status,
	pounds,
}) => (
	<View style={[itemStyles.claimItemContainer, styles.roundedShadowBox]}>
		<View style={itemStyles.claimItemContent}>
			<View style={itemStyles.claimItemMainContent}>
				<View style={itemStyles.claimItemDepartureRow}>
					<View style={itemStyles.claimItemRow}>
						<Ionicons
							name="ios-pin"
							size={16}
							color="white"
							style={{ marginLeft: 1, marginRight: 10 }}
						/>
						<Image source={location} style={{ position: 'absolute' }} />
						<Text style={[itemStyles.claimGrayLabel]}>{departureTime}</Text>
						<Text style={[itemStyles.claimMainText, { paddingLeft: 10 }]}>
							{from}
						</Text>
					</View>
					<Text
						style={[
							itemStyles.claimGrayLabel,
							{ paddingLeft: 10, textAlign: 'right' },
						]}
					>
						{date}
					</Text>
				</View>
				<Image
					source={dots}
					style={{ position: 'absolute', top: 16, left: 10 }}
				/>
				<View style={itemStyles.claimItemArrivalRow}>
					<View style={itemStyles.claimItemRow}>
						<Ionicons
							name="ios-time"
							size={14}
							style={{ marginRight: 10 }}
							color="white"
						/>
						<Image source={delay} style={{ position: 'absolute', left: 0.5 }} />
						<Text style={[itemStyles.claimGrayLabel]}>{arrivalTime}</Text>
						<Text style={[itemStyles.claimMainText, { paddingLeft: 10 }]}>
							{to}
						</Text>
					</View>
					<Text
						style={[
							itemStyles.claimGrayLabel,
							{ paddingLeft: 10, textAlign: 'right', color: orange },
						]}
					>
						{duration} mins
					</Text>
				</View>
			</View>
			<View
				style={[
					itemStyles.claimItemFooter,
					{ backgroundColor: getStatusColor(status) },
				]}
			>
				<Text
					style={[
						itemStyles.claimItemStatus,
						{
							color:
								status === 'Claimed' || status === 'Claim'
									? gray
									: iOSColors.white,
						},
					]}
				>
					{status}
				</Text>
				<Text
					style={[
						itemStyles.claimItemPounds,
						{
							color:
								status === 'Claimed' || status === 'Claim'
									? blue
									: iOSColors.white,
						},
					]}
				>
					&pound;
					{pounds}
				</Text>
			</View>
		</View>
	</View>
);

export default ClaimItem;
