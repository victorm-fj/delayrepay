import React, { Component } from 'react';
import { View, Image, ActivityIndicator, Platform } from 'react-native';
import AWS from 'aws-sdk';
import { Button } from 'react-native-elements';
import { Buffer } from 'buffer';

import styles from '../styles';

// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'eu-west-1';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId: 'eu-west-1:3ef5035e-7ed3-444b-8463-f2668dcd887e',
});

const rekognition = new AWS.Rekognition();
const detectText = (base64Image) =>
	new Promise((resolve, reject) => {
		const params = {
			Image: {
				Bytes: Buffer.from(base64Image, 'base64'),
			},
		};
		rekognition.detectText(params, (err, data) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(data);
		});
	});

class ImageResult extends Component {
	state = { image: '', loading: true };

	componentDidMount() {
		const { navigation } = this.props;
		navigation.setParams({ onImageContinue: this.onContinueHandler });
		const image = navigation.getParam('imageData', '');
		this.setState({ image, loading: false });
	}

	onContinueHandler = async () => {
		const { navigation } = this.props;
		const { image } = this.state;
		try {
			let result = null;
			this.setState({ loading: true });
			if (Platform.OS === 'android') {
				result = navigation.getParam('textValues', null);
			} else {
				result = await detectText(image);
			}
			console.log('rekognition returned data', result);
			navigation.replace('TextDetection', { result });
		} catch (error) {
			console.log('rekognition detectText error', error);
			this.setState({ loading: false });
		}
	};

	render() {
		const { image, loading } = this.state;
		if (loading) {
			return (
				<View style={styles.loaderContainer}>
					<ActivityIndicator size="large" color="#0000ff" />
				</View>
			);
		}
		return (
			<View style={styles.container}>
				<View style={styles.mainContainer}>
					{image !== '' ? (
						<Image
							style={styles.modalImage}
							source={{ uri: `data:image/jpeg;base64,${image}` }}
							resizeMode="contain"
						/>
					) : null}
				</View>
				<View style={styles.actionsContainer}>
					<Button
						title="Confirm"
						onPress={this.onContinueHandler}
						titleStyle={styles.buttonTitleStyle}
						buttonStyle={styles.buttonStyle}
						containerStyle={styles.buttonContainerStyle}
					/>
				</View>
			</View>
		);
	}
}

export default ImageResult;
