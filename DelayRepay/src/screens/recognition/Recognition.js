import React, { Component } from 'react';
import ReactNative, { View, UIManager } from 'react-native';
import CoreMLImage from 'react-native-core-ml-image';

import styles from '../styles';

class Recognition extends Component {
	state = {
		bestMatch: { identifier: 'no-train-ticket', confidence: 0.99 },
	};

	componentDidUpdate(prevProps, prevState) {
		const {
			bestMatch: { identifier },
		} = this.state;
		if (
			prevState.bestMatch.identifier !== identifier &&
			identifier === 'train-ticket'
		) {
			setTimeout(() => this.onTicketFound(), 1000);
		}
	}

	onTicketFound() {
		const {
			bestMatch: { identifier },
		} = this.state;
		if (this.coreML && identifier === 'train-ticket') {
			UIManager.dispatchViewManagerCommand(
				ReactNative.findNodeHandle(this.coreML),
				UIManager.CoreMLImage.Commands.takePhoto,
				[]
			);
		}
	}

	onCapturedPhoto(imageData: string) {
		const { navigation } = this.props;
		navigation.replace('ImageResult', { imageData });
	}

	onClassification(classifications) {
		let bestMatch = null;
		if (classifications && classifications.length > 0) {
			// Loop through all of the classifications and find the best match
			classifications.forEach((classification) => {
				if (!bestMatch || classification.confidence > bestMatch.confidence) {
					bestMatch = classification;
				}
			});
			// Is best match confidence better than our threshold?
			if (bestMatch.confidence > 0.9) {
				this.setState((prevState) => {
					if (bestMatch.identifier !== prevState.bestMatch.identifier) {
						return { bestMatch };
					}
					return null;
				});
			}
		}
	}

	render() {
		const {
			bestMatch: { identifier },
		} = this.state;
		const borderColor = identifier === 'train-ticket' ? 'green' : 'red';
		return (
			<View style={styles.container}>
				<CoreMLImage
					ref={(coreML) => (this.coreML = coreML)}
					modelFile="TrainTicketOrNotTrainTicket"
					onClassification={(evt) => this.onClassification(evt)}
					onCapturedPhoto={(evt) => this.onCapturedPhoto(evt)}
				>
					<View style={[styles.scannerContainer, { justifyContent: 'center' }]}>
						<View
							style={[
								styles.scanner,
								{ flexDirection: 'row', flexWrap: 'wrap' },
							]}
						>
							<View style={[styles.modelTopLeftMark, { borderColor }]} />
							<View style={[styles.modelTopRightMark, { borderColor }]} />
							<View style={[styles.modelBottomLeftMark, { borderColor }]} />
							<View style={[styles.modelBottomRightMark, { borderColor }]} />
						</View>
					</View>
				</CoreMLImage>
			</View>
		);
	}
}

export default Recognition;
