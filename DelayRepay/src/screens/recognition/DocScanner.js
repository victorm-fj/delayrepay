// @flow
import React from 'react';
import { View, Platform } from 'react-native';
import DocumentScanner from 'react-native-document-scanner';
import type { NavigationScreenProp } from 'react-navigation';
import Orientation from 'react-native-orientation';

import styles from '../styles';

const RNCamera =
	Platform.OS === 'android' && require('react-native-camera').RNCamera;

const ticketTypes = [
	'Advanced Day',
	'Advanced Single',
	'Advanced Return',
	'Anytime Day',
	'Anytime Single',
	'Anytime Return',
	'Off-Peak',
	'Super Off-Peak',
	'Off-Peak Day',
	'Super Off-Peak Day',
	'Seven Days',
	'A month',
	'Over one month',
	'Rover',
	'Ranger',
];

const ticketClasses = [
	'Adult Standard Class',
	'Child Standard Class',
	'Adult First Class',
	'Child First Class',
];

type Props = { navigation: NavigationScreenProp<*> };
class DocScanner extends React.Component<Props> {
	componentDidMount() {
		if (Platform.OS === 'android') {
			Orientation.lockToLandscape();
		}
	}

	componentWillUnmount() {
		if (Platform.OS === 'android') {
			Orientation.unlockAllOrientations();
		}
	}

	onPictureTakenHandler = (data: Object) => {
		const { navigation } = this.props;
		navigation.replace('ImageResult', { imageData: data.croppedImage });
	};

	onTextRecognizedHandler = ({ textBlocks }) => {
		const textValues = textBlocks.map((block) => block.value);
		console.log('textBlock values', textValues);
		let isTicketType = false;
		let isTicketClass = false;
		if (textValues.length > 10) {
			textValues.forEach((value) => {
				if (ticketTypes.includes(value)) {
					isTicketType = true;
				}
				if (ticketClasses.includes(value)) {
					isTicketClass = true;
				}
			});
			if (isTicketType && isTicketClass) {
				this.takePicture(textValues);
			}
		}
	};

	takePicture = async (textValues) => {
		if (this.camera) {
			const options = { quality: 0.33, base64: true, height: 300, width: 400 };
			const data = await this.camera.takePictureAsync(options);
			console.log('data', data);
			const { navigation } = this.props;
			navigation.replace('ImageResult', {
				imageData: data.base64,
				textValues,
			});
		}
	};

	renderAndroidScanner() {
		const { navigation } = this.props;
		return (
			<RNCamera
				ref={(camera) => (this.camera = camera)}
				style={styles.docScannerContainer}
				type={RNCamera.Constants.Type.back}
				permissionDialogTitle="Permission to use camera"
				permissionDialogMessage="We need your permission to use your camera phone"
				onTextRecognized={
					navigation.isFocused() ? this.onTextRecognizedHandler : null
				}
			/>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				{Platform.OS === 'android' ? (
					this.renderAndroidScanner()
				) : (
					<DocumentScanner
						useBase64
						onPictureTaken={this.onPictureTakenHandler}
						overlayColor="rgba(189,189,189,0.33)"
						quality={0.33}
						detectionCountBeforeCapture={2}
						style={styles.docScannerContainer}
					/>
				)}
			</View>
		);
	}
}

export default DocScanner;
