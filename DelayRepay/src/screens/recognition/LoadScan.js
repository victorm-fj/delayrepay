// @flow
import React from 'react';
import DeviceInfo from 'react-native-device-info';
import type { NavigationScreenProp } from 'react-navigation';

import Loader from '../../components/Loader';

type Props = { navigation: NavigationScreenProp<*> };
class LoadScan extends React.Component<Props> {
	componentDidMount() {
		const { navigation } = this.props;
		const model = DeviceInfo.getModel();
		if (model === 'iPhone X' || model === 'iPhone 8' || model === 'iPhone 8S') {
			return navigation.navigate('Recognition');
		}
		return navigation.navigate('DocScanner');
	}

	render() {
		return <Loader />;
	}
}

export default LoadScan;
