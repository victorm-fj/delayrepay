import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { sanFranciscoSpacing } from 'react-native-typography';

import { claimsData } from './mockData';
import ClaimItem from '../home/ClaimItem';

const activeColor = 'rgba(40,60,80,1)';
const inactiveColor = 'rgba(40,60,80,0.5)';
class Claims extends Component {
	state = { show: 'all' };

	getData = () => {
		const { show } = this.state;
		if (show === 'claimed') {
			return claimsData.filter((claim) => claim.status === 'Claimed');
		}
		if (show === 'refunded') {
			return claimsData.filter((claim) => claim.status === 'Refunded');
		}
		if (show === 'rejected') {
			return claimsData.filter((claim) => claim.status === 'Rejected');
		}
		return claimsData;
	};

	renderItem = ({ item }) => <ClaimItem key={item.id} {...item} />;

	render() {
		const { show } = this.state;
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: 'rgba(250,250,250,1)',
				}}
			>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
						paddingHorizontal: 20,
						marginTop: 55,
						width: '100%',
						alignItems: 'center',
					}}
				>
					<TouchableOpacity
						onPress={() => this.props.navigation.goBack()}
						style={{ padding: 5 }}
					>
						<Ionicons name="ios-arrow-back" size={28} />
					</TouchableOpacity>
					<Text
						style={{
							fontFamily: 'SF Pro Text',
							fontSize: 17,
							lineHeight: 20,
							letterSpacing: sanFranciscoSpacing(-0.41),
							color: 'rgba(40,60,80,1)',
						}}
					>
						Claims
					</Text>
					<TouchableOpacity onPress={() => {}}>
						<Ionicons name="ios-contact" size={28} />
					</TouchableOpacity>
				</View>
				<View
					style={{
						flexDirection: 'row',
						alignItems: 'center',
						paddingHorizontal: 20,
						marginTop: 50,
						marginBottom: 14,
					}}
				>
					<TouchableOpacity onPress={() => this.setState({ show: 'all' })}>
						<Text
							style={{
								fontFamily: 'SF Pro Text',
								fontSize: 12,
								lineHeight: 14,
								letterSpacing: sanFranciscoSpacing(-0.13),
								color: show === 'all' ? activeColor : inactiveColor,
								marginRight: 30,
							}}
						>
							All
						</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.setState({ show: 'claimed' })}>
						<Text
							style={{
								fontFamily: 'SF Pro Text',
								fontSize: 12,
								lineHeight: 14,
								letterSpacing: sanFranciscoSpacing(-0.13),
								color: show === 'claimed' ? activeColor : inactiveColor,
								marginRight: 30,
							}}
						>
							Claimed
						</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.setState({ show: 'refunded' })}>
						<Text
							style={{
								fontFamily: 'SF Pro Text',
								fontSize: 12,
								lineHeight: 14,
								letterSpacing: sanFranciscoSpacing(-0.13),
								color: show === 'refunded' ? activeColor : inactiveColor,
								marginRight: 30,
							}}
						>
							Refunded
						</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.setState({ show: 'rejected' })}>
						<Text
							style={{
								fontFamily: 'SF Pro Text',
								fontSize: 12,
								lineHeight: 14,
								letterSpacing: sanFranciscoSpacing(-0.13),
								color: show === 'rejected' ? activeColor : inactiveColor,
								marginRight: 30,
							}}
						>
							Rejected
						</Text>
					</TouchableOpacity>
				</View>
				<FlatList
					contentContainerStyle={{
						flexGrow: 1,
						paddingBottom: 20,
						backgroundColor: 'white',
					}}
					data={this.getData()}
					extraData={claimsData}
					keyExtractor={(item, index) => `${item.id}-${index}`}
					renderItem={this.renderItem}
				/>
			</View>
		);
	}
}

export default Claims;
