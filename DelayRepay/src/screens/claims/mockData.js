export const claimsData = [
	{
		id: 0,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Claimed',
		pounds: 297.55,
	},
	{
		id: 1,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Claimed',
		pounds: 297.55,
	},
	{
		id: 2,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Refunded',
		pounds: 297.55,
	},
	{
		id: 3,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Refunded',
		pounds: 297.55,
	},
	{
		id: 4,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Rejected',
		pounds: 297.55,
	},
	{
		id: 5,
		date: '24/08/18',
		duration: 197,
		from: 'Birmingham New Street',
		departureTime: '23:59',
		to: 'London Kings Cross',
		arrivalTime: '23:59',
		status: 'Rejected',
		pounds: 297.55,
	},
];
