import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export const dimensions = {
	fullHeight: height,
	fullWidth: width,
};

export const colors = {
	background: '#f3f5f8',
	action: '#3b7cff',
};

export const padding = {
	sm: 10,
	md: 20,
	lg: 30,
	xl: 40,
};

export const fonts = {
	sm: 12,
	md: 18,
	lg: 28,
	regular: 'Gotham-Book',
	light: 'Gotham-Light',
	bold: 'Gotham-Bold',
	paragraph: 14,
};

const baseStyles = {
	screenContainer: {
		flex: 1,
		alignItems: 'center',
		padding: padding.sm,
		paddingBottom: padding.md,
		backgroundColor: colors.background,
	},
	scrollViewContent: {
		flexGrow: 1,
		alignItems: 'center',
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: padding.sm,
	},
	footnoteTextStyle: {
		fontSize: fonts.sm,
		fontFamily: fonts.regular,
		margin: padding.sm,
		textAlig: 'center',
	},
};

export default (overrides = {}) =>
	StyleSheet.create({ ...baseStyles, ...overrides });
