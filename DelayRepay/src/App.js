/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import BackgroundGeolocation from 'react-native-background-geolocation';
import NotificationsIOS from 'react-native-notifications';
import Amplify from 'aws-amplify';

import awsmobile from './aws-exports';
import Nav from './nav/Nav';
import geofences from './geofences';

Amplify.configure(awsmobile);

type Props = {};
class App extends Component<Props> {
	async componentDidMount() {
		// Background geolocation event listeners
		BackgroundGeolocation.on('location', this.onLocation, this.onError);
		// BackgroundGeolocation.on('motionchange', this.onMotionChange);
		// BackgroundGeolocation.on('activitychange', this.onActivityChange);
		// BackgroundGeolocation.on('providerchange', this.onProviderChange);
		BackgroundGeolocation.on('geofence', this.onGeoFence);
		try {
			// Execute ready method
			const state = await BackgroundGeolocation.ready({
				// Geolocation Config
				desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_LOW,
				distanceFilter: 30,
				// Activity Recognition
				stopTimeout: 1,
				// Application config
				activityRecognitionInterval: 10000,
				// debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
				logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
				stopOnTerminate: false, // <-- Allow the background-service to continue tracking when user closes the app.
				startOnBoot: true, // <-- Auto start tracking when device is powered-up.,
				// reset: true, // <-- true to always apply the supplied config
				preventSuspend: true,
				heartbeatInterval: 60,
				disableMotionActivityUpdates: true, // do not use CMMotionActivityManager so Health & Fitness permission will not be requested
			});
			console.log('- BackgroundGeolocation is ready: ', state);
			await BackgroundGeolocation.addGeofences(geofences);
			if (!state.enabled) {
				// 3. Start tracking
				await BackgroundGeolocation.startGeofences();
				// await BackgroundGeolocation.start();
			}
		} catch (error) {
			console.log('- BackgroundGeolocation error: ', error);
		}
	}

	componentWillUnmount() {
		BackgroundGeolocation.removeListeners();
	}

	onLocation = (location) => {
		console.log('- [event] location: ', location);
	};

	onError = (error) => {
		console.warn('- [event] location error ', error);
	};

	onActivityChange = (activity) => {
		console.log('- [event] activitychange: ', activity);
	};

	onProviderChange = (provider) => {
		console.log('- [event] providerchange: ', provider);
	};

	onMotionChange = (location) => {
		console.log('- [event] motionchange: ', location.isMoving, location);
	};

	onGeoFence = (geofence) => {
		console.log('- [event] geofence: ', geofence);
		const { identifier } = geofence;
		NotificationsIOS.localNotification({
			alertBody: `You are in ${identifier}`,
			alertTitle: 'Geofence alert',
			userInfo: {},
		});
	};

	render() {
		return <Nav />;
	}
}

export default App;
