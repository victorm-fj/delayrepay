import ChatBot from './ChatBot';

export { default as Loading } from './steps/common/loading/Loading';
export default ChatBot;
