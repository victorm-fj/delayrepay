import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	chatbotContainer: {
		overflow: 'hidden',
		height: '100%',
		width: '100%',
	},
	scrollContainer: {
		flexGrow: 1,
		justifyContent: 'flex-end',
		padding: 0,
		paddingTop: 10,
		paddingBottom: 20,
	},
	footerContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		padding: 15,
		height: 54,
		backgroundColor: '#f9fafc',
		borderBottomWidth: 0,
		shadowColor: '#3b4859',
		shadowOpacity: 0.35,
		shadowOffset: { height: 0.5 },
		shadowRadius: 1.5,
		elevation: 4,
	},
	textInputStyle: {
		flex: 1,
		borderWidth: 0,
		backgroundColor: '#f3f5f8',
		color: '#3b4859',
		fontSize: 14,
		fontFamily: 'GothamRounded-Book',
		paddingHorizontal: 15,
		height: 34,
		borderRadius: 4,
	},
	sendContainerStyle: {
		height: 34,
	},
	sendIconStyle: {
		width: 30,
		height: '100%',
	},
	textStyle: {
		fontSize: 14,
		fontFamily: 'GothamRounded-Book',
		lineHeight: 18,
	},
	optionsStyle: {
		marginTop: 20,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		alignItems: 'center',
	},
	optionContainerStyle: {
		margin: 4,
		minHeight: 28,
	},
	optionElementStyle: {
		paddingHorizontal: 8,
		paddingTop: 4,
		borderRadius: 30,
		borderWidth: 1,
	},
	loaderContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
