import schema from './schemas/schema';

/**
 * Takes ChatBot props and returns an object containint defaultUserSettings
 * and steps array. Iterates over every step to set settings according
 * to the type of step and to the predefined settings from props; it also
 * parses and validates the structure of every predefined step
 * @param { object } props - ChatBot's props
 * @returns { object } { defaultUserSettings, steps }
 */
export const getSteps = (props: Object) => {
	const {
		botDelay,
		botBubbleColor,
		botFontColor,
		customDelay,
		customLoadingColor,
		userDelay,
		userBubbleColor,
		userFontColor,
	} = props;
	const defaultBotSettings = {
		delay: botDelay,
		bubbleColor: botBubbleColor,
		fontColor: botFontColor,
	};
	const defaultUserSettings = {
		delay: userDelay,
		bubbleColor: userBubbleColor,
		fontColor: userFontColor,
	};
	const defaultCustomSettings = {
		delay: customDelay,
		loadingColor: customLoadingColor,
	};
	const steps = {};
	props.steps.forEach((step) => {
		let settings = {};
		if (step.user) {
			settings = defaultUserSettings;
		} else if (step.message || step.asMessage || step.options) {
			settings = defaultBotSettings;
		} else if (step.component) {
			settings = defaultCustomSettings;
		}
		steps[step.id] = Object.assign({}, settings, schema.parse(step));
	});
	return { defaultUserSettings, steps };
};

/**
 * Takes an step object and renderesSteps array and returns true if the
 * current step is last step in conversation, depending on wheter it's
 * a bot's or user's message
 * @param { object } step - Current step in conversation
 * @param { array } renderedSteps - All rendered steps in screen
 * @returns { boolean } isLastPosition
 */
export const isLastPosition = (step: Object, renderedSteps: Array<Object>) => {
	const len = renderedSteps.length;
	const stepIndex = renderedSteps.map((s) => s.key).indexOf(step.key);
	if (len <= 1 || stepIndex + 1 === len) {
		return true;
	}
	const nextStep = renderedSteps[stepIndex + 1];
	const hasMessage = nextStep.message || nextStep.asMessage;
	if (!hasMessage) {
		return true;
	}
	const isLast = step.user !== nextStep.user;
	return isLast;
};

/**
 * Takes an step object and renderesSteps array and returns true if the
 * current step is first step in conversation, depending on wheter it's
 * a bot's or user's message
 * @param { object } step - Current step in conversation
 * @param { array } renderedSteps - All rendered steps in screen
 * @returns { boolean } isFirstPosition
 */
export const isFirstPosition = (step: Object, renderedSteps: Array<Object>) => {
	const stepIndex = renderedSteps.map((s) => s.key).indexOf(step.key);
	if (stepIndex === 0) {
		return true;
	}
	const lastStep = renderedSteps[stepIndex - 1];
	const hasMessage = lastStep.message || lastStep.asMessage;
	if (!hasMessage) {
		return true;
	}
	const isFirst = step.user !== lastStep.user;
	return isFirst;
};

// @flow
/**
 * Returns boolean value according to the number of allowed
 * attempts the user has done, in this case allowedAttempts
 * number invalid attemps are forgiven, and at the third we
 * show prompts to the user
 * @param { array } renderedSteps
 * @param { string } stepCheckPointId
 * @param { number } allowedAttempts
 * @param { string } invalidStepId
 * @param { string } [secondStepCheckPointId='']
 * @returns { boolean }
 */
export const triggerOptions = (
	renderedSteps: Array<Object>,
	stepCheckPointId: string,
	allowedAttempts: number,
	invalidStepId: string,
	secondStepCheckPointId?: string = ''
) => {
	let attempts = 0;
	for (let index = renderedSteps.length - 1; index >= 0; index -= 1) {
		const step = renderedSteps[index];
		if (step.id === stepCheckPointId || step.id === secondStepCheckPointId) {
			if (attempts >= allowedAttempts) {
				return true;
			}
			return false;
		}
		if (step.id === invalidStepId) {
			attempts += 1;
		}
	}
	return false;
};
