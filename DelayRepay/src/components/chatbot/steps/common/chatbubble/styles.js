import { StyleSheet } from 'react-native';
// import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
	item: {
		marginTop: 5, // moderateScale(5, 2),
		flexDirection: 'row',
		minHeight: 28,
	},
	itemIn: {
		marginLeft: 15,
	},
	itemOut: {
		alignSelf: 'flex-end',
		marginRight: 15,
	},
	balloon: {
		maxWidth: 300, // moderateScale(300, 2),
		paddingHorizontal: 10, // moderateScale(10, 2),
		justifyContent: 'center',
		borderRadius: 20,
	},
	arrowContainer: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: -1,
		flex: 1,
	},
	arrowLeftContainer: {
		justifyContent: 'flex-end',
		alignItems: 'flex-start',
	},

	arrowRightContainer: {
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
	},

	arrowLeft: {
		left: -4, // moderateScale(-4, 0.5),
	},

	arrowRight: {
		right: -4, // moderateScale(-4, 0.5),
	},
});
