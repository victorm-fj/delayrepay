// @flow
import * as React from 'react';
import { View } from 'react-native';

import styles from './styles';
import Bubble from './Bubble';

type Props = {
	user: boolean,
	children: React.Node,
	isLast: boolean,
	botBubbleColor: string,
	userBubbleColor: string,
};
const ChatBubble = ({
	user,
	children,
	isLast,
	botBubbleColor,
	userBubbleColor,
}: Props) => (
	<View>
		{!user ? (
			<Bubble
				itemStyle={styles.itemIn}
				backgroundColor={botBubbleColor}
				isLast={isLast}
				arrowContainerStyle={styles.arrowLeftContainer}
				arrowStyle={styles.arrowLeft}
				arrowPath="M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z"
			>
				{children}
			</Bubble>
		) : (
			<Bubble
				itemStyle={styles.itemOut}
				backgroundColor={userBubbleColor}
				isLast={isLast}
				arrowContainerStyle={styles.arrowRightContainer}
				arrowStyle={styles.arrowRight}
				arrowPath="M48,35c-7-4-6-8.75-6-17.5C28,17.5,29,35,48,35z"
			>
				{children}
			</Bubble>
		)}
	</View>
);

export default ChatBubble;
