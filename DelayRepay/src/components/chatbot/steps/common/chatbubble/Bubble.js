// @flow
import * as React from 'react';
import { View, ViewPropTypes } from 'react-native';
import Svg, { Path } from 'react-native-svg';
// import { moderateScale } from 'react-native-size-matters';

import styles from './styles';

type Props = {
	isLast: boolean,
	itemStyle: ViewPropTypes.style,
	backgroundColor: string,
	children: React.Node,
	arrowContainerStyle: ViewPropTypes.style,
	arrowStyle: ViewPropTypes.style,
	arrowPath: string,
};
type State = { height: number };
class Bubble extends React.Component<Props, State> {
	state = { height: 28 };

	onLayoutHandler = ({
		nativeEvent: {
			layout: { height },
		},
	}: Object): void => {
		this.setState({ height });
	};

	render() {
		const {
			isLast,
			itemStyle,
			backgroundColor,
			children,
			arrowContainerStyle,
			arrowStyle,
			arrowPath,
		} = this.props;
		const { height } = this.state;
		return (
			<View style={[styles.item, itemStyle]} onLayout={this.onLayoutHandler}>
				<View
					style={[
						styles.balloon,
						{
							backgroundColor,
							paddingTop: height > 28 ? 6 : 4, // moderateScale(6, 2) : moderateScale(4, 2),
							paddingBottom: height > 28 ? 4 : 2, // moderateScale(4, 2) : moderateScale(2, 2),
						},
					]}
				>
					<View>{children}</View>
					{isLast && (
						<View style={[styles.arrowContainer, arrowContainerStyle]}>
							<Svg
								style={arrowStyle}
								// width={moderateScale(15.5, 0.6)}
								// height={moderateScale(17.5, 0.6)}
								width={15.5}
								height={17.5}
								viewBox="32.484 17.5 15.515 17.5"
								enable-background="new 32.485 17.5 15.515 17.5"
							>
								<Path d={arrowPath} fill={backgroundColor} x="0" y="0" />
							</Svg>
						</View>
					)}
				</View>
			</View>
		);
	}
}

export default Bubble;
