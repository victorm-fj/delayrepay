// @flow
import React from 'react';
import { Animated, ART } from 'react-native';

import AnimatedCircle from './AnimatedCircle';

const { Surface } = ART;

type Props = {
	size?: number,
	color?: string,
	spaceBetween?: number,
	activeColor?: string,
};
type State = { circles: Array<*> };
class Bubbles extends React.Component<Props, State> {
	timers = [];

	unmounted = false;

	static defaultProps = {
		size: 8,
		color: '#8392a7',
		spaceBetween: 4,
		activeColor: '#3b4859',
	};

	state = {
		circles: [
			new Animated.Value(0),
			new Animated.Value(0),
			new Animated.Value(0),
		],
	};

	componentDidMount() {
		const { circles } = this.state;
		circles.forEach((val, index) => {
			const timer = setTimeout(() => this.animate(index), index * 300);
			this.timers.push(timer);
		});
	}

	componentWillUnmount() {
		this.timers.forEach((timer) => clearTimeout(timer));
		this.unmounted = true;
	}

	animate(index: number) {
		const { circles } = this.state;
		Animated.sequence([
			Animated.timing(circles[index], {
				toValue: 0.5,
				duration: 450,
			}),
			Animated.timing(circles[index], {
				toValue: 1,
				duration: 450,
			}),
			Animated.timing(circles[index], {
				toValue: 0.5,
				duration: 450,
			}),
		]).start(() => {
			if (!this.unmounted) {
				this.animate(index);
			}
		});
	}

	renderBubble(index: number) {
		const { size, spaceBetween, color, activeColor } = this.props;
		const { circles } = this.state;
		const scale = circles[index];
		const offset = {
			x: size + index * (size * 2 + spaceBetween),
			y: size,
		};
		const interColor = scale.interpolate({
			inputRange: [0, 1],
			outputRange: [color, activeColor],
		});
		const interRadius = scale.interpolate({
			inputRange: [0, 1],
			outputRange: [size * 0.8, size],
		});
		return (
			<AnimatedCircle
				fill={interColor}
				radius={interRadius}
				scale={scale}
				{...offset}
			/>
		);
	}

	render() {
		const { size, spaceBetween } = this.props;
		const width = size * 6 + spaceBetween * 2;
		const height = size * 2;
		return (
			<Surface width={width} height={height}>
				{this.renderBubble(0)}
				{this.renderBubble(1)}
				{this.renderBubble(2)}
			</Surface>
		);
	}
}

export default Bubbles;
