// @flow
import React from 'react';
import { ART } from 'react-native';

const { Shape, Path } = ART;

type Props = { radius: number, opacity: number };
class CircleShape extends React.Component<Props> {
	render() {
		const { radius } = this.props;
		const path = Path()
			.moveTo(0, -radius)
			.arc(0, radius * 2, radius)
			.arc(0, radius * -2, radius)
			.close();
		return <Shape {...this.props} d={path} />;
	}
}

export default CircleShape;
