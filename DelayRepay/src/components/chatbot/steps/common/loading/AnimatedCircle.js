import { Animated } from 'react-native';
import CircleShape from './CircleShape';

const AnimatedCircle = Animated.createAnimatedComponent(CircleShape);

export default AnimatedCircle;
