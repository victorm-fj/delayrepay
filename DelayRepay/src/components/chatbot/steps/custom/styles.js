import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	customStepContainer: {
		backgroundColor: 'transparent',
		paddingVertical: 15,
	},
});
