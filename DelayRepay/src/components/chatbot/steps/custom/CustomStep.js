// @flow
import React from 'react';

import ChatBubble from '../common/chatbubble/ChatBubble';

type Props = {
	delay: number,
	step: Object,
	steps: Object,
	previousStep: Object,
	renderedSteps: Array<Object>,
	triggerNextStep: Function,
	botBubbleColor: string,
	userBubbleColor: string,
};
class CustomStep extends React.Component<Props> {
	componentDidMount() {
		const { delay, step, triggerNextStep } = this.props;
		const delayed = typeof step.delay === 'undefined' ? delay : step.delay;
		const { waitAction } = step;
		setTimeout(() => {
			if (!waitAction) {
				triggerNextStep();
			}
		}, delayed);
	}

	renderComponent = () => {
		const {
			step,
			steps,
			previousStep,
			triggerNextStep,
			renderedSteps,
		} = this.props;
		const { component } = step;
		return React.cloneElement(component, {
			step,
			steps,
			previousStep,
			renderedSteps,
			triggerNextStep,
		});
	};

	render() {
		const { botBubbleColor, userBubbleColor } = this.props;
		return (
			<ChatBubble
				user={false}
				botBubbleColor={botBubbleColor}
				userBubbleColor={userBubbleColor}
				isLast
			>
				{this.renderComponent()}
			</ChatBubble>
		);
	}
}

export default CustomStep;
