import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	textStepContainer: {
		alignItems: 'flex-end',
		width: '100%',
	},
	imageContainer: {
		marginTop: 6,
		marginRight: 6,
		marginBottom: 10,
		marginLeft: 6,
		padding: 2,
		backgroundColor: '#fff',
		borderTopRightRadius: 21,
		borderTopLeftRadius: 21,
		borderWidth: 1,
		borderColor: '#ddd',
	},
	imageStyle: {
		height: 40,
		width: 40,
		borderRadius: 20,
	},
	bubbleTextStyle: {
		fontSize: 14,
		fontFamily: 'GothamRounded-Book',
		lineHeight: 18,
	},
	timestampsStyle: {
		fontFamily: 'GothamRounded-Book',
		fontSize: 12,
		color: '#8392a7',
		marginHorizontal: 15,
		marginTop: 5,
		marginBottom: 3,
	},
});
