// @flow
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Emoji from '@ardentlabs/react-native-emoji';
import moment from 'moment';

import ChatBubble from '../common/chatbubble/ChatBubble';
import Loading from '../common/loading/Loading';
import styles from './styles';

type Props = {
	botBubbleColor: string,
	userBubbleColor: string,
	botFontColor: string,
	userFontColor: string,
	isFirst: boolean,
	isLast: boolean,
	step: Object,
	triggerNextStep: Function,
	bubbleStyle: Object,
	previousStep?: Object,
	previousValue?: any,
	steps?: Object,
	renderedSteps?: Array<Object>,
};
type State = { loading: boolean };
class TextStep extends Component<Props, State> {
	static defaultProps = {
		previousStep: {},
		steps: {},
		previousValue: '',
		renderedSteps: [],
	};

	state = {
		loading: true,
	};

	componentDidMount() {
		const { step, triggerNextStep } = this.props;
		const { component, delay, waitAction, user } = step;
		const isComponentWatingUser = component && waitAction;
		if (!user) {
			setTimeout(() => {
				this.setState({ loading: false });
				if (!isComponentWatingUser) {
					triggerNextStep();
				}
			}, delay);
		} else {
			this.setState({ loading: false });
			if (!isComponentWatingUser) {
				triggerNextStep();
			}
		}
	}

	renderMessage = () => {
		const { loading } = this.state;
		const { previousValue, step, botFontColor, userFontColor } = this.props;
		const { component, fontColor, user } = step;
		let { message } = step;
		if (loading && !user) {
			return <Loading color={fontColor} />;
		}
		if (component) {
			const {
				steps,
				previousStep,
				triggerNextStep,
				renderedSteps,
			} = this.props;
			return React.cloneElement(component, {
				step,
				steps,
				previousStep,
				renderedSteps,
				triggerNextStep,
			});
		}
		message = message.replace(/{previousValue}/g, previousValue);
		const messageLength = message.length;
		const emojiStartIndex = message.indexOf('[');
		const emojiEndIndex = message.indexOf(']');
		let textMessage = message;
		let emojiName = '';
		let textMessage2 = null;
		const startEmojiString = message.slice(emojiStartIndex);
		if (emojiStartIndex > 0) {
			// There is an emoji in the message
			if (emojiEndIndex + 1 === messageLength) {
				// Emoji is the last part of the message
				textMessage = message.slice(0, emojiStartIndex - 1);
				emojiName = startEmojiString.slice(12, -2);
			} else {
				// Emoji is in the middle of the message
				textMessage = message.slice(0, emojiStartIndex - 1);
				textMessage2 = message.slice(emojiEndIndex + 2);
				const add = startEmojiString.indexOf(']') - 1;
				emojiName = startEmojiString.slice(
					12,
					-(messageLength - emojiStartIndex) + add
				);
			}
		}
		return (
			<Text
				style={[
					styles.bubbleTextStyle,
					{ color: step.user ? userFontColor : botFontColor },
				]}
			>
				{textMessage} {emojiName && <Emoji name={emojiName} />}
				{textMessage2}
			</Text>
		);
	};

	render() {
		const { loading } = this.state;
		const {
			step,
			isFirst,
			isLast,
			bubbleStyle,
			botBubbleColor,
			userBubbleColor,
		} = this.props;
		const { user } = step;
		return (
			<View>
				<View
					style={[
						styles.textStepContainer,
						{ flexDirection: user ? 'row-reverse' : 'row' },
					]}
				>
					<ChatBubble
						style={bubbleStyle}
						user={user}
						botBubbleColor={botBubbleColor}
						userBubbleColor={userBubbleColor}
						isFirst={isFirst}
						isLast={isLast}
					>
						{this.renderMessage()}
					</ChatBubble>
				</View>
				{!loading &&
					!step.component &&
					isLast &&
					step.timestamps && (
					<Text
						style={[
							styles.timestampsStyle,
							{
								alignSelf: user ? 'flex-end' : 'flex-start',
							},
						]}
					>
						{moment(step.timestamps).calendar(null, {
							sameDay: '[Today,] LT',
						})}
					</Text>
				)}
			</View>
		);
	}
}

export default TextStep;
