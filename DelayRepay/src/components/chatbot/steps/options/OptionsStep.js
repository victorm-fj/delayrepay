// @flow
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import styles from './styles';

type Props = {
	step: Object,
	triggerNextStep: Function,
	bubbleStyle: Object,
	speak: Function,
	permissions: Object,
	highlightedColor: string,
	highlightedFontColor: string,
};
class OptionsStep extends React.Component<Props> {
	onOptionClick = ({ value }: { value: string }) => {
		const { triggerNextStep } = this.props;
		triggerNextStep({ value });
	};

	renderOption = (option: Object) => {
		const { bubbleStyle, highlightedColor, highlightedFontColor } = this.props;
		const { value, label, highlighted } = option;
		return (
			<TouchableOpacity
				key={value}
				onPress={() => this.onOptionClick({ value })}
				style={styles.optionContainerStyle}
			>
				<View
					style={[
						styles.optionElementStyle,
						bubbleStyle,
						{
							borderColor: highlightedColor,
							backgroundColor: highlighted ? highlightedColor : 'transparent',
						},
					]}
				>
					<Text
						style={[
							styles.textStyle,
							{ color: highlighted ? highlightedFontColor : highlightedColor },
						]}
					>
						{label}
					</Text>
				</View>
			</TouchableOpacity>
		);
	};

	render() {
		const {
			step: { options },
		} = this.props;
		return (
			<View style={styles.optionsStyle}>{options.map(this.renderOption)}</View>
		);
	}
}

export default OptionsStep;
