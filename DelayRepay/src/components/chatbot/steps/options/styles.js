import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	textStyle: {
		fontSize: 14,
		// fontFamily: 'Gotham-Book',
		lineHeight: 18,
	},
	optionsStyle: {
		marginTop: 20,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		alignItems: 'center',
	},
	optionContainerStyle: {
		margin: 2.5,
	},
	optionElementStyle: {
		minHeight: 28,
		paddingHorizontal: 10,
		paddingTop: 4,
		borderRadius: 30,
		borderWidth: 1,
	},
});
