/**
 * @format
 * @flow
 */
import * as React from 'react';
import { Alert, AppState } from 'react-native';
import Permissions from 'react-native-permissions';
import forIn from 'lodash.forin';

const messageBuilder = (deniedPermissions: Array<string>) => {
	let message = 'Please grant "DelayRepay" permissions to handle ';
	if (deniedPermissions.length === 3) {
		message +=
			deniedPermissions[0].charAt(0).toUpperCase() +
			deniedPermissions[0].slice(1);
		message += ', ';
		message +=
			deniedPermissions[1].charAt(0).toUpperCase() +
			deniedPermissions[1].slice(1);
		message += ' and ';
		message +=
			deniedPermissions[2].charAt(0).toUpperCase() +
			deniedPermissions[2].slice(1);
		message += '.';
	} else if (deniedPermissions.length === 2) {
		message +=
			deniedPermissions[0].charAt(0).toUpperCase() +
			deniedPermissions[0].slice(1);
		message += ' and ';
		message +=
			deniedPermissions[1].charAt(0).toUpperCase() +
			deniedPermissions[1].slice(1);
		message += '.';
	} else {
		message +=
			deniedPermissions[0].charAt(0).toUpperCase() +
			deniedPermissions[0].slice(1);
		message += '.';
	}
	return message;
};

export default (
	WrappedComponent: React.ComponentType<Object>,
	permissions: Array<string>
) => {
	type Props = {};
	type State = { permissions: Object };
	class RequirePermissions extends React.Component<Props, State> {
		state = {
			permissions: permissions.reduce(
				(acc, val) => ({
					...acc,
					[val]: 'undetermined',
				}),
				{}
			),
		};

		componentDidMount() {
			AppState.addEventListener('change', this.handleAppStateChange);
			this.updatePermissions();
		}

		componentWillUnmount() {
			AppState.removeEventListener('change', this.handleAppStateChange);
		}

		handleAppStateChange = (appState: string) => {
			if (appState === 'active') {
				this.updatePermissions();
			}
		};

		openSettings = async () => {
			try {
				await Permissions.openSettings();
				// alert('Back to app!');
			} catch (error) {
				console.log('openSettings error', error);
			}
		};

		async alertToOpenSettings(deniedPermissions: Array<string>) {
			const message = messageBuilder(deniedPermissions);
			Alert.alert('Denied Permissions', message, [
				{ text: 'Cancel', onPress: () => {}, style: 'cancel' },
				{ text: 'OK', onPress: this.openSettings },
			]);
		}

		async updatePermissions() {
			try {
				let response;
				if (permissions.length > 1) {
					response = await Permissions.checkMultiple(permissions);
					this.setState({ permissions: response });
				} else {
					const permission = permissions[0];
					if (permission === 'location') {
						response = await Permissions.check(permission, { type: 'always' });
					} else {
						response = await Permissions.check(permission);
					}
					this.setState({ permissions: { [permission]: response } });
				}
			} catch (error) {
				console.log('chechPermissions error', error);
				Alert.alert('Error', 'Something went wrong!', [
					{
						text: 'Ok',
						onPress: () => {},
					},
				]);
			} finally {
				const undeterminedPermissions = [];
				const deniedPermissions = [];
				forIn(this.state.permissions, (value, key) => {
					if (value === 'undetermined') {
						undeterminedPermissions.push(key);
					} else if (value === 'denied') {
						deniedPermissions.push(key);
					}
				});
				if (undeterminedPermissions.length > 0) {
					this.requestPermissions(undeterminedPermissions);
				} else if (deniedPermissions.length > 0) {
					this.alertToOpenSettings(deniedPermissions);
				}
			}
		}

		requestPermissions(undeterminedPermissions: Array<string>) {
			undeterminedPermissions.forEach(async (permission) => {
				await this.requestPermission(permission);
			});
		}

		async requestPermission(permission: string) {
			let response = '';
			if (permission === 'location') {
				response = await Permissions.request(permission, { type: 'always' });
			} else {
				response = await Permissions.request(permission);
			}
			this.setState((prevState) => ({
				permissions: { ...prevState.permissions, [permission]: response },
			}));
		}

		render() {
			return (
				<WrappedComponent {...this.props} status={this.state.permissions} />
			);
		}
	}
	return RequirePermissions;
};
