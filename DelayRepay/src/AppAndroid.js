/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import Amplify from 'aws-amplify';

import awsmobile from './aws-exports';
import Nav from './nav/Nav';

Amplify.configure(awsmobile);

type Props = {};
class App extends Component<Props> {
	render() {
		return <Nav />;
	}
}

export default App;
