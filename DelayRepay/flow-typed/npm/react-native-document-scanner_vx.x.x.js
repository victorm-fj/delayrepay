// flow-typed signature: 667ef8c9e2a1314c4e3bbae84a18c48c
// flow-typed version: <<STUB>>/react-native-document-scanner_v^1.4.2/flow_v0.75.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-native-document-scanner'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-native-document-scanner' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-native-document-scanner/Example/__tests__/index.android' {
  declare module.exports: any;
}

declare module 'react-native-document-scanner/Example/__tests__/index.ios' {
  declare module.exports: any;
}

declare module 'react-native-document-scanner/Example/index.android' {
  declare module.exports: any;
}

declare module 'react-native-document-scanner/Example/index.ios' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-native-document-scanner/Example/__tests__/index.android.js' {
  declare module.exports: $Exports<'react-native-document-scanner/Example/__tests__/index.android'>;
}
declare module 'react-native-document-scanner/Example/__tests__/index.ios.js' {
  declare module.exports: $Exports<'react-native-document-scanner/Example/__tests__/index.ios'>;
}
declare module 'react-native-document-scanner/Example/index.android.js' {
  declare module.exports: $Exports<'react-native-document-scanner/Example/index.android'>;
}
declare module 'react-native-document-scanner/Example/index.ios.js' {
  declare module.exports: $Exports<'react-native-document-scanner/Example/index.ios'>;
}
declare module 'react-native-document-scanner/index' {
  declare module.exports: $Exports<'react-native-document-scanner'>;
}
declare module 'react-native-document-scanner/index.js' {
  declare module.exports: $Exports<'react-native-document-scanner'>;
}
