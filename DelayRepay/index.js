/** @format */

import { AppRegistry, Platform } from 'react-native';
import App from './src/App';
import AppAndroid from './src/AppAndroid';
import { name as appName } from './app.json';

// To use background geolocation lib for android we need to purchase a license
// For testing purposes don't user background geolocation in android version
const app = Platform.OS === 'android' ? AppAndroid : App;

AppRegistry.registerComponent(appName, () => app);
